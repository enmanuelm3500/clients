package com.example.clientback.service;

import com.example.clientback.entity.Client;

import java.util.List;

public interface ClientService {

    List<Client> getList();

    Client create(Client client);

    Client findById(Integer id);

    void delete(Integer id);


}
