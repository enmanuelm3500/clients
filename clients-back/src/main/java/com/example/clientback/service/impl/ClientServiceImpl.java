package com.example.clientback.service.impl;

import com.example.clientback.entity.Client;
import com.example.clientback.exception.DataIntegrityException;
import com.example.clientback.repository.ClientRepository;
import com.example.clientback.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    private static final String NOT_REGISTRY = "no se encontro el registro id: %s";


    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getList() {
        return (List<Client>) clientRepository.findAll();
    }

    @Override
    public Client create(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public Client findById(Integer id) {
        return clientRepository.findById(id).orElseThrow(() -> new DataIntegrityException(NOT_REGISTRY,id));
    }

    @Override
    public void delete(Integer id) {
        clientRepository.delete(findById(id));
    }
}
