package com.example.clientback.repository;

import com.example.clientback.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String userName);
}
