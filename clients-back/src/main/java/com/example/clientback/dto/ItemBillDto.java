package com.example.clientback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class ItemBillDto extends AbstracDto {


    @JsonProperty
    @NotNull
    private Integer quantity;

    @JsonProperty
    @NotNull
    private ProductDto products;

    public ItemBillDto() {
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ProductDto getProducts() {
        return products;
    }

    public void setProducts(ProductDto products) {
        this.products = products;
    }
}
