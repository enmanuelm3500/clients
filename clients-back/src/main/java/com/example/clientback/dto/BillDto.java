package com.example.clientback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

public class BillDto extends AbstracDto{

    @JsonProperty
    private String description;

    @JsonProperty
    private String observations;

    @JsonProperty
    private Date createDate;

    @JsonProperty
    @NotNull
    private List<ItemBillDto> itemBillList;


    public BillDto() { }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<ItemBillDto> getItemBillList() {
        return itemBillList;
    }

    public void setItemBillList(List<ItemBillDto> itemBillList) {
        this.itemBillList = itemBillList;
    }
}
