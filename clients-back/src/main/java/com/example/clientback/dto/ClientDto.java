package com.example.clientback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

public class ClientDto extends AbstracDto {


    @JsonProperty
    @NotEmpty
    @NotNull
    private String name;

    @JsonProperty
    @NotEmpty
    @NotNull
    private String addres;

    @JsonProperty
    @NotEmpty
    @NotNull
    private String email;

    @JsonProperty
    private Date created;

    @JsonProperty
    private String photo;

    public ClientDto() {
    }

    public ClientDto(@NotEmpty @NotNull String name, @NotEmpty @NotNull String addres, @NotEmpty @NotNull String email, Date created, String photo) {
        this.name = name;
        this.addres = addres;
        this.email = email;
        this.created = created;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
