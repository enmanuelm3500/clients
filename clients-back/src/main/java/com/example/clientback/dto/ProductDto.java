package com.example.clientback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class ProductDto extends AbstracDto {

    @JsonProperty
    @NotNull
    @NotEmpty
    private String name;

    @JsonProperty
    @NotNull
    @NotEmpty
    private Double price;

    @JsonProperty
    private Date cratedate;

    public ProductDto() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getCratedate() {
        return cratedate;
    }

    public void setCratedate(Date cratedate) {
        this.cratedate = cratedate;
    }
}
