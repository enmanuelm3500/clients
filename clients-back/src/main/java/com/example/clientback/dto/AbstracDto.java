package com.example.clientback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AbstracDto {

    @JsonProperty
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
