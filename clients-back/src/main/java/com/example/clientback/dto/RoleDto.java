package com.example.clientback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RoleDto extends AbstracDto {

    @JsonProperty
    private String authority;

    public RoleDto() {
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
