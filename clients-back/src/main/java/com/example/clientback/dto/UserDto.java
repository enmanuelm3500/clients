package com.example.clientback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;

public class UserDto extends AbstracDto {

    @JsonProperty
    private String username;

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
