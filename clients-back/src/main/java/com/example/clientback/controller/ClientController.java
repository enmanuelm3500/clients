package com.example.clientback.controller;

import com.example.clientback.controller.mappers.ClientMapper;
import com.example.clientback.dto.ClientDto;
import com.example.clientback.service.ClientService;
import org.mapstruct.factory.Mappers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"}, methods = {RequestMethod.GET,RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.POST})
@RestController
@RequestMapping("/client")
public class ClientController {

    private ClientMapper clientMapper;

    private ClientService clientService;

    public ClientController(ClientService clientService) {
        setClientService(clientService);
        setClientMapper(Mappers.getMapper(ClientMapper.class));
    }

    @GetMapping("/list")
    public List<ClientDto> getList(Principal principal) {
        return clientMapper.toDtoList(clientService.getList());
    }

    @PostMapping("/create")
    public ResponseEntity<ClientDto> create(Principal principal, @RequestBody ClientDto clientDto) {
        return new ResponseEntity<>(clientMapper.toDto(clientService.create(clientMapper.toEntity(clientDto))),HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ClientDto update(Principal principal, @RequestBody ClientDto clientDto) {
        return clientMapper.toDto(clientService.create(clientMapper.toEntity(clientDto)));
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<ClientDto> findById(@Valid @PathVariable Integer id){
        return new ResponseEntity<>(clientMapper.toDto(clientService.findById(id)),HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@Valid @PathVariable Integer id){
        clientService.delete(id);
    }

    public void setClientMapper(ClientMapper clientMapper) {
        this.clientMapper = clientMapper;
    }

    public ClientMapper getClientMapper() {
        return clientMapper;
    }

    public ClientService getClientService() {
        return clientService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }
}
