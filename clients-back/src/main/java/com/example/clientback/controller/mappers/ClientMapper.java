package com.example.clientback.controller.mappers;

import com.example.clientback.dto.ClientDto;
import com.example.clientback.entity.Client;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ClientMapper {

    ClientDto toDto(Client entity);

    Client toEntity(ClientDto dto);

    List<ClientDto> toDtoList(List<Client> clients);


}
