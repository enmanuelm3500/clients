package com.example.clientback.exception;

public class ApiKeyMissingException extends ApiKeyValidationException {

    public ApiKeyMissingException(String cause) {
        super(cause);
    }
}
