package com.example.clientback.exception;

import static java.lang.String.format;

public class AuthorizationException extends RestApplicationException {

    private static final String ERROR_CODE = "ERROR.AUTHORIZATION";
    private static final String ERROR_MESSAGE = "No estás autorizado para acceder a %s.";
    private static final String DEVELOPER_MESSAGE = "Resource Not Found. You are not authorized to the resource.";

    public AuthorizationException() {
        super(ERROR_CODE, ERROR_MESSAGE, DEVELOPER_MESSAGE);
    }

    public AuthorizationException(String format, Object... args) {
        super(ERROR_CODE, format(format, args), DEVELOPER_MESSAGE);
    }

    public AuthorizationException(String identifier) {
        this(ERROR_MESSAGE, identifier);
    }
}
