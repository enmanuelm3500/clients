package com.example.clientback.exception;

import com.example.clientback.entity.AbstractEntity;

import static java.lang.String.format;

public class DataIntegrityException extends RestApplicationException {

    private static final String ERROR_CODE = "ERROR.DATA.INTEGRITY.VIOLATION";
    private static final String ERROR_MESSAGE = "No se puede realizar la actualización de %s porque afecta la integridad de datos.";
    private static final String DEVELOPER_MESSAGE = "Data Integrity violation.";

    public DataIntegrityException() {
        super(ERROR_CODE, ERROR_MESSAGE, DEVELOPER_MESSAGE);
    }

    public DataIntegrityException(String format, Object... args) {
        super(ERROR_CODE, format(format, args), DEVELOPER_MESSAGE);
    }

    public DataIntegrityException(AbstractEntity entity) {
        this(format(ERROR_MESSAGE, entity));
    }
}
