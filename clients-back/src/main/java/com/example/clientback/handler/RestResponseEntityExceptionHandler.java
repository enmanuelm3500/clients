package com.example.clientback.handler;

import com.example.clientback.dto.ApiErrorDTO;
import com.example.clientback.dto.ValidationErrorDTO;
import com.example.clientback.exception.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.List;

import static com.example.clientback.util.HttpHeaderHelper.getHttpHeaders;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.util.StringUtils.capitalize;

/**
 * Clase que controla como procesar las excepciones en la capa controller.
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger HANDLER_LOGGER = LogManager.getLogger();

    @Override
    protected ResponseEntity<Object> handleBindException(
            BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleExceptionInternal(ex, ApiErrorDTO.ApiErrorDTOBuilder.create(), BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final ApiErrorDTO.ApiErrorDTOBuilder builder = ApiErrorDTO.ApiErrorDTOBuilder.create()
                .withMessage("Solicitud invalida.")
                .withDetail(getValidationErrorDTOList(ex))
                .withDevMessage(String.format("%s is not valid", capitalize(ex.getParameter().getParameterName())));
        return handleExceptionInternal(ex, builder, BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final ApiErrorDTO.ApiErrorDTOBuilder builder = ApiErrorDTO.ApiErrorDTOBuilder.create()
                .withMessage("Solicitud invalida.");
        return handleExceptionInternal(ex, builder, BAD_REQUEST, request);
    }

    /**
     * BAD_REQUEST 400
     */
    @ExceptionHandler({RestApplicationException.class, EntityMappingException.class, ApiKeyMissingException.class})
    public ResponseEntity<ApiErrorDTO> handleBadRequest(RestApplicationException ex) {
        return handleExceptionInternal(ex, BAD_REQUEST);
    }

    /**
     * NOT_FOUND 404
     */
    @ExceptionHandler(value = EntityNotFoundException.class)
    protected ResponseEntity<ApiErrorDTO> handleNotFound(EntityNotFoundException ex) {
        return handleExceptionInternal(ex, NOT_FOUND);
    }

    /**
     * CONFLICT 409
     */
    @ExceptionHandler({DuplicateEntityException.class, DataIntegrityException.class})
    protected ResponseEntity<ApiErrorDTO> handleConflict(RestApplicationException ex) {
        return handleExceptionInternal(ex, CONFLICT);
    }

    /**
     * SERVER_ERROR 500
     */
    @ExceptionHandler({NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class})
    public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        final ApiErrorDTO.ApiErrorDTOBuilder builder = ApiErrorDTO.ApiErrorDTOBuilder.create()
                .withMessage("Error crítico.");
        return handleExceptionInternal(ex, builder, getHeaders(request), INTERNAL_SERVER_ERROR, request);
    }

    /**
     * SERVER_ERROR 500
     */
    @ExceptionHandler({org.springframework.dao.DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleJPAConflic(final RuntimeException ex) {
        final ApiErrorDTO.ApiErrorDTOBuilder builder = ApiErrorDTO.ApiErrorDTOBuilder.create()
                .withMessage(String.format("Error al intentar guardar informacion en base de datos: %s", ex.getMessage()))
                .withStatus(CONFLICT)
                .withErrorCode("DATABASE.CONFLICT")
                .withThrowable(ex)
                .withDevMessage(ex.getMessage());
        HANDLER_LOGGER.error("error al intentar guardar los datos", ex.getMessage());
        return new ResponseEntity<>(builder.build(), CONFLICT);
    }


    /**
     * UNAUTHORIZED 403
     */
    @ExceptionHandler({ApiKeyValidationException.class})
    public ResponseEntity<ApiErrorDTO> handleApiKeyValidationException(ApiKeyValidationException ex) {
        return handleExceptionInternal(ex, UNAUTHORIZED);
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<ApiErrorDTO> handleAccessDeniedException(AccessDeniedException ex) {
        return handleExceptionInternal(new ApiAccessDeniedException(ex), UNAUTHORIZED);
    }

    private ResponseEntity<Object> handleExceptionInternal(
            Exception ex, ApiErrorDTO.ApiErrorDTOBuilder builder, HttpStatus status, WebRequest request) {
        final ApiErrorDTO body = builder
                .withStatus(status)
                .withThrowable(ex)
                .build();
        return handleExceptionInternal(ex, body, null, status, request);
    }

    private ResponseEntity<ApiErrorDTO> handleExceptionInternal(
            RestApplicationException ex, HttpStatus status) {
        final ApiErrorDTO.ApiErrorDTOBuilder builder = ApiErrorDTO.ApiErrorDTOBuilder.create()
                .withStatus(status)
                .withErrorCode(ex.getErrorCode())
                .withMessage(ex.getMessage())
                .withDevMessage(ex.getDevMessage())
                .withThrowable(ex);
        return new ResponseEntity<>(builder.build(), status);
    }

    private HttpHeaders getHeaders(WebRequest request) {
        try {
            return getHttpHeaders(request);
        } catch (WebRequestException innerException) {
            HANDLER_LOGGER.error(innerException.getMessage(), innerException);
            return new HttpHeaders();
        }
    }

    private List<ValidationErrorDTO> getValidationErrorDTOList(MethodArgumentNotValidException ex) {
        List<ValidationErrorDTO> validationErrorDTOList = new ArrayList<>();
        ex.getBindingResult().getFieldErrors().forEach(
                value -> validationErrorDTOList.add(new ValidationErrorDTO(
                        value.getObjectName(),
                        value.getField(),
                        value.getRejectedValue(),
                        value.getDefaultMessage()))
        );
        return validationErrorDTOList;
    }
}
