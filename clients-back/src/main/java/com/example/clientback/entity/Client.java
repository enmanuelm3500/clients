package com.example.clientback.entity;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "client")
public class Client extends AbstractEntity implements Serializable {

    @NotEmpty
    @Column(name = "name", length = 30, nullable = false)
    private String name;

    @NotEmpty
    private String addres;

    @Email
    @NotEmpty
    private String email;

    @NotNull
    @Column(name = "date_created")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date created;

    @Column(name = "photo")
    private String photo;

    @PrePersist
    public void prePersist() {
        created = new Date();
    }


    public Client(@NotEmpty String name, @NotEmpty String addres, @Email @NotEmpty String email, @NotNull Date created, String photo) {
        this.name = name;
        this.addres = addres;
        this.email = email;
        this.created = created;
        this.photo = photo;
    }

    public Client() {
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
