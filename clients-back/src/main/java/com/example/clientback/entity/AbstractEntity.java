package com.example.clientback.entity;


import javax.persistence.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Entidad base para todas las entidades del proyecto
 */
@MappedSuperclass
public class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Map<String, String> getUniqueFields() {
        final Map<String, String> columns = new HashMap<>();
        Arrays.asList(this.getClass().getDeclaredFields()).forEach(field -> {
            Column column = field.getAnnotation(Column.class);
            if (column != null && column.unique()) {
                columns.put(field.getName(), column.name());
            }
        });
        return columns;
    }
}
