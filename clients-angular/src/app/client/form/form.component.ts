import {Component, OnInit} from '@angular/core';
import {Client} from '../client';
import {ClientService} from '../client.service';
import {ActivatedRoute, Router} from '@angular/router';
import * as _swal from 'sweetalert';
import {SweetAlert} from 'sweetalert/typings/core';

const swal: SweetAlert = _swal as any;


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private client: Client = new Client();
  private titulo: string = 'Crear Cliente';

  constructor(
    private clientService: ClientService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.getClient();
  }

  private create() {
    console.log('cliked');
    console.log(this.client);
    this.clientService.create(this.client).subscribe(response => {
      swal('Success', 'Registro creado', 'success');
      this.router.navigate(['clients']);
    });
  }

  private update() {
    console.log(this.client);
    this.clientService.update(this.client).subscribe(response => {
      swal('Success', 'Registro editado', 'success');
      this.router.navigate(['clients']);
    });
  }

  private getClient() {
    this.activatedRoute.params.subscribe(value => {
      let id = value['id'];
      if (id) {
        this.clientService.getClient(id).subscribe(response => {
          this.client = response;
        });
      }
    });
  }

}
