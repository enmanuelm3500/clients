import {Injectable} from '@angular/core';
import {Client} from './client';
import {Observable, throwError} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import * as _swal from 'sweetalert';
import {SweetAlert} from 'sweetalert/typings/core';

const swal: SweetAlert = _swal as any;

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private url: string = 'http://localhost:8080/client';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(
    private http: HttpClient,
  ) {
  }

  getClients(): Observable<Client[]> {
    return this.http.get(this.url + '/list').pipe(
      map((response) => response as Client[])
    );
    //return this.http.get<Client[]>(this.url);
    //return of(CLIENTS);
  }

  create(client: Client): Observable<Client> {
    return this.http.post<Client>(this.url + '/create', client, {headers: this.httpHeaders}).pipe(
      catchError(err => {
          console.log('err');
          console.log(err);
          swal('Error', err.status + ': ' + err.error.cause, 'error');
          return throwError(err);
        }
      )
    );
  }

  update(client: Client): Observable<Client> {
    return this.http.put<Client>(this.url + '/update', client, {headers: this.httpHeaders}).pipe(
      catchError(err => {
          console.log('err');
          console.log(err);
          swal('Error', err.status + ': ' + err.error.cause, 'error');
          return throwError(err);
        }
      )
    );
  }

  getClient(id: string): Observable<Client> {
    return this.http.get<Client>(this.url + '/find/' + id).pipe(
      catchError(err => {
          console.log('err');
          console.log(err);
          swal('Error', err.status + ': ' + err.error.cause, 'error');
          return throwError(err);
        }
      )
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(this.url + '/delete/' + id).pipe(
      catchError(err => {
          console.log('err');
          console.log(err);
          swal('Error', err.status + ': ' + err.error.cause, 'error');
          return throwError(err);
        }
      )
    );
  }
}
