import {Component, OnInit} from '@angular/core';
import {Client} from './client';
import {ClientService} from './client.service';
import {Router} from '@angular/router';
// ES6 Modules or TypeScript
import Swal from 'sweetalert2';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  clients: Client[];

  /*clients: Client[] = [
    {name: 'enmanuel', address: 'mavarez', createAt: 'hoy', id: '1', email: 'enmanuelm3500@gmail.com'},
    {name: 'Juan', address: 'Mavarez', createAt: 'hoy', id: '2', email: 'enmanuelm3500@gmail.com'},
    {name: 'Maria', address: 'Guaidot', createAt: 'hoy', id: '1', email: 'enmanuelm3500@gmail.com'},
    {name: 'enmanuel', address: 'mavarez', createAt: 'hoy', id: '1', email: 'enmanuelm3500@gmail.com'},
    {name: 'enmanuel', address: 'mavarez', createAt: 'hoy', id: '1', email: 'enmanuelm3500@gmail.com'}
  ];*/

  constructor(private clientService: ClientService,
              private router: Router,
  ) {

  }

  ngOnInit() {
    //this.clients = CLIENTS;
    this.clientService.getClients().subscribe(response => {
      this.clients = response;
      console.log(this.clients);
    });
  }

  delete(id) {
    Swal.fire({
      title: 'Esta seguro?',
      text: 'Esta seguro que desea eliminar este registro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar!',
      cancelButtonText: 'No, cancelar!',
    }).then((result) => {
      if (result.value) {
        this.clientService.delete(id).subscribe(response => {
          console.log(response);
          Swal.fire(
            'Success!',
            'Registro eliminado.',
            'success'
          );
          this.clientService.getClients().subscribe((response) => {
            this.clients = response;
          });
        });
      }
    });


    /*this.clientService.delete(id).subscribe(response => {
        this.clientService.getClients().subscribe(response => {
          this.clients = response;
        });
      }
    );*/
  }

}
