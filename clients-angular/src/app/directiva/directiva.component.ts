import {Component} from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.css']
})
export class DirectivaComponent {

  coursesList: string[] = ['Java', 'JavaScript', 'PHP', 'C#'];
  showCourses: boolean = true;

  constructor() {
  }

  setShowCourses(): void {
    this.showCourses = (this.showCourses === true) ? false : true;
  }

}
